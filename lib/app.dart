import 'package:flutter/material.dart';
import 'package:healt_app/src/pages/home.dart';
import 'package:healt_app/src/pages/profile.dart';
import 'package:healt_app/src/pages/video.dart';
import 'package:healt_app/src/services/config.dart';
import 'package:healt_app/src/services/sharedPreferences.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }


  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: TabBarView(controller: _tabController, children: <Widget>[
        HomePage(),
        Video(),
        ProfilePage()
      ]),
      bottomNavigationBar: Material(
        color: ColorApp.PrimaryDark,
        child: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.menu),
              text: 'Menu',
            ),
            Tab(
              icon: Icon(Icons.video_library),
              text: 'Video',
            ),
            Tab(
              icon: Icon(Icons.people),
              text: 'Tentang Saya',
            )
          ],
        ),
      ),
    );
  }
}
