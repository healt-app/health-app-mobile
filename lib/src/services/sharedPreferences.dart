import 'dart:convert';
import 'package:healt_app/src/services/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

Future setVersion() async {
  final prefs = await SharedPreferences.getInstance();
  String url = NetworkApp.version;
  var res = await http.get(Uri.encodeFull(url));
  var content = json.decode(res.body);
  int version = content['payload']['version'];
  prefs.setInt('version', version);
}

Future ceckVersion() async {
  final prefs = await SharedPreferences.getInstance();
  int currentVersion = prefs.getInt('version');
  String url = NetworkApp.version;
  var res = await http.get(Uri.encodeFull(url));
  var content = json.decode(res.body);
  int version = content['payload']['version'];
  if (version != currentVersion) {
    print(
        'dataaaaaaaaaaaaaaaaaaaaaaaaa terhapussssssssssssssssssssssssssssssssssssss');
    prefs.clear();
  } else {
    print(currentVersion);
    print('=================================================================================');
    print(version);
  }
}

Future versioning() async {
  final prefs = await SharedPreferences.getInstance();
  bool cek = prefs.containsKey('version');
  if (cek) {
    ceckVersion();
  } else {
    print(
        'initiallllllllllllllll dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    setVersion();
  }
}

Future<String> getDataSP(String key) async {
  versioning();
  final prefs = await SharedPreferences.getInstance();
  bool cek = prefs.containsKey(key);
  if (cek) {
    return json.decode(prefs.getString(key));
  } else {
    return null;
  }
}

Future<String> setDataSP(String key, value) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setString(key, json.encode(value));
  return 'data with key = ' + key + 'has been saved !!!';
}
