import 'package:flutter/material.dart';

// const baseUrl = 'http://192.168.17.1:4000/';
const baseUrl = 'https://healt-app-api.herokuapp.com/';
// const middleWareUrl = 'http://192.168.17.1:3000/';
const middleWareUrl = 'https://healt-app.herokuapp.com/';
const token = 'ini tempat token';

class NetworkApp {
  static const imageUrl = baseUrl + 'ftp/uploads/';
  static const listMenu = baseUrl + 'list-menu';
  static const detailMenu = middleWareUrl + 'detail-menu/';
  static const listVideo = baseUrl + 'video';
  static const profile = baseUrl + 'profile';
  static const settings = baseUrl + 'setting';
  static const version = baseUrl + 'version';
}

class ColorApp {
  static const PrimaryDark = Color.fromRGBO(27,94,32 ,1);
  static const Primary = Color.fromRGBO(76,175,80 ,1);
  static const Secondary = Color.fromRGBO(63,81,181,1);
  static const Light = Color.fromRGBO(250,250,250 ,1); 
  static const LightGrey = Color.fromRGBO(224,224,224 ,1);
}

class StringApp {
  static const TitleApp = 'Aplikasi Tips Kesehatan';
  static const TitleMenuVideo = 'Video Tips Kesehatan';
  static const TitleMenuProfile = 'Profile';
}
