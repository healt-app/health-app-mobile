import 'package:flutter/material.dart';
import 'package:flutter_inappbrowser/flutter_inappbrowser.dart';
import 'package:healt_app/src/services/config.dart';

class InAppBrowserMenu extends StatefulWidget {

  final String idMenu;

  const InAppBrowserMenu({Key key, this.idMenu}) : super(key: key);

  @override
  _InAppBrowserMenuState createState() => new _InAppBrowserMenuState();
}

class _InAppBrowserMenuState extends State<InAppBrowserMenu> {
  InAppWebViewController webView;
  String url = "";
  double progress = 0;

  @override
  void initState() {
    super.initState();
    setState(() {
     url = NetworkApp.detailMenu + widget.idMenu; 
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          (progress != 1.0) ? LinearProgressIndicator(value: progress) : null,
          Expanded(
            child: InAppWebView(
              initialUrl: url,
              initialHeaders: {},
              initialOptions: {},
              onWebViewCreated: (InAppWebViewController controller) {
                webView = controller;
              },
              onLoadStart: (InAppWebViewController controller, String url) {
                print(widget.idMenu);
                setState(() {
                  this.url = url;
                });
              },
              onProgressChanged:
                  (InAppWebViewController controller, int progress) {
                setState(() {
                  this.progress = progress / 100;
                });
              },
            ),
          ),
        ].where((Object o) => o != null).toList(),
      ),
    );
  }
}
