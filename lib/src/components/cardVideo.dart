
import 'package:flutter/material.dart';
import 'package:healt_app/src/services/config.dart';

class VideoCard extends StatelessWidget {
  final String title;
  final String description;
  final String image;

  VideoCard({this.title, this.image, this.description});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [BoxShadow(spreadRadius: 1, blurRadius: 5)]),
      child: Row(
        children: <Widget>[
          Flexible(
              child: Column(
            children: <Widget>[
              Flexible(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Text(this.title,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Text(
                          this.description,
                          textAlign: TextAlign.center,
                        ))
                  ],
                ),
              ),
              Flexible(
                  child: IconButton(
                icon: Icon(
                  Icons.play_circle_filled,
                  color: Colors.red,
                  size: 30,
                ),
                onPressed: () {},
              )),
            ],
          )),
          Flexible(
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  image: DecorationImage(
                      image: NetworkImage(NetworkApp.imageUrl + this.image),
                      fit: BoxFit.cover)),
            ),
          ),
        ],
      ),
    );
  }
}
