import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:healt_app/src/components/loading/loadingMenuCard.dart';
import 'package:healt_app/src/pages/detail.dart';
import 'package:healt_app/src/services/config.dart';
import 'package:http/http.dart' as http;

class CardMenu extends StatelessWidget {
  final int idMenu;
  final String title;
  final String image;

  CardMenu({this.idMenu, this.title, this.image});

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.all(10),
        color: ColorApp.Primary,
        elevation: 10,
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Column(
          children: <Widget>[
            Flexible(
              flex: 6,
              child: Stack(children: <Widget>[
                Positioned.fill(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.red,
                        image: DecorationImage(
                            image:
                                NetworkImage(NetworkApp.imageUrl + this.image),
                            fit: BoxFit.cover)),
                  ),
                ),
                Positioned.fill(
                  child: Opacity(
                    opacity: 0.5,
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailPage(
                                  idMenu: this.idMenu.toString(),
                                  title: this.title),
                            ),
                          );
                        },
                        splashColor: Colors.green,
                      ),
                    ),
                  ),
                )
              ]),
            ),
            Flexible(
              flex: 1,
              child: Container(
                alignment: Alignment.center,
                color: Colors.pink,
                child: Text(
                  this.title,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 12),
                ),
              ),
            ),
          ],
        ));
  }
}

class SliverCardMenu extends StatefulWidget {
  @override
  _SliverCardMenuState createState() => _SliverCardMenuState();
}

class _SliverCardMenuState extends State<SliverCardMenu> {
  List data = [];

  Future getMenu() async {
    String url = NetworkApp.listMenu;

    var res = await http.get(Uri.encodeFull(url));
    var content = json.decode(res.body);
    setState(() {
      data = content['payload'];
    });
  }

  // initial state
  @override
  void initState() {
    super.initState();
    this.getMenu();
  }

  Widget build(BuildContext context) {
    if (data.length >= 1) {
      return SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1 / 1,
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return CardMenu(
                idMenu: data[index]['id_menu'],
                title: data[index]['title'],
                image: data[index]['image_menu']);
          },
          childCount: data.length,
        ),
      );
    } else {
      return SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1 / 1,
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return LoadingMenuCard();
          },
          childCount: 4,
        ),
      );
    }
  }
}
