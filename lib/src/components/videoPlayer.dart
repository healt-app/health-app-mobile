import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:youtube_player/youtube_player.dart';

class VideoPlayer extends StatelessWidget {
  
  final String youtubeID;

  VideoPlayer({this.youtubeID});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: YoutubePlayer(
          context: context,
          source: this.youtubeID,
          quality: YoutubeQuality.MEDIUM,
          startFullScreen: true,
        ),
      ),
    );
  }
}
