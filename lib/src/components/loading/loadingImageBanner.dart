import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:healt_app/src/services/config.dart';
import 'package:healt_app/src/services/sharedPreferences.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ImageBanner extends StatefulWidget {
  @override
  _ImageBannerState createState() => _ImageBannerState();
}

class _ImageBannerState extends State<ImageBanner> {
  String banner;

  Future getSetting() async {
    String url = NetworkApp.settings;
    var res = await http.get(Uri.encodeFull(url));
    setDataSP('banner', res.body);
    var content = json.decode(res.body);
    banner = content['payload']['banner_image'];
    setState(() {});
  }

  Future getBanner() async {
    var data = await getDataSP('banner');
    if (data == null) {
      getSetting();
    } else {
      var content = json.decode(data);
      banner = content['payload']['banner_image'];
      setState(() {});
    }
  }

  // initial state
  @override
  void initState() {
    super.initState();
    this.getBanner();
  }

  Widget build(BuildContext context) {
    return banner == null
        ? Container(
            child: null,
          )
        : Image(
            image: NetworkImage(NetworkApp.imageUrl + banner),
            fit: BoxFit.cover,
          );
  }
}
