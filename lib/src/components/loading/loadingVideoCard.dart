import 'package:flutter/material.dart';

class LoadingVideoCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.grey[400],
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [BoxShadow(spreadRadius: 1, blurRadius: 5, color: Colors.grey[700])]),
    );
  }
}
