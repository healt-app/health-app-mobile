import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:healt_app/src/components/videoPlayer.dart';

class DetailVideo extends StatelessWidget {
  final String idVideo;
  final String title;

  const DetailVideo({Key key, this.idVideo, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.title),
      ),
      body: VideoPlayer(youtubeID: this.idVideo),
    );
  }
}
