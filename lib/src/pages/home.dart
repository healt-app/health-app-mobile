import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:healt_app/src/components/cardMenu.dart';
import 'package:healt_app/src/components/loading/loadingImageBanner.dart';
import 'package:healt_app/src/services/config.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(child: ListMenu()),
    );
  }
}

class ListMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: ColorApp.PrimaryDark ,
          pinned: true,
          elevation: 10,
          expandedHeight: 400.0,
          flexibleSpace: FlexibleSpaceBar(
              title: Text(
                StringApp.TitleApp,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    shadows: [
                      Shadow(
                        blurRadius: 10.0,
                        color: Colors.black,
                        offset: Offset(2.0, 2.0),
                      ),
                      Shadow(
                        blurRadius: 10.0,
                        color: Colors.black,
                        offset: Offset(-2.0, -2.0),
                      )
                    ]),
              ),
              collapseMode: CollapseMode.parallax,
              centerTitle: true,
              background: ImageBanner()),
        ),
        SliverCardMenu(),
        SliverPadding(
          padding: EdgeInsets.all(200),
          sliver: null,
        )
      ],
    );
  }
}
