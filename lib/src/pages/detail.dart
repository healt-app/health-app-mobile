import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:healt_app/src/components/inAppBrowser.dart';

class DetailPage extends StatelessWidget {
  final String idMenu;
  final String title;

  const DetailPage({Key key, this.idMenu, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.title),
      ),
      body: InAppBrowserMenu(idMenu: this.idMenu),
    );
  }
}
