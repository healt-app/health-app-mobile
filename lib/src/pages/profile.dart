import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:healt_app/src/services/config.dart';
import 'package:http/http.dart' as http;

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var data = {};
  String innerHTML = '<p>Loading ...</p>';

  Future getListVideo() async {
    String url = NetworkApp.profile;

    var res = await http.get(Uri.encodeFull(url));
    var content = json.decode(res.body);
    var html = json.decode(content['payload']['content_menu']);
    setState(() {
      data = content['payload'];
      innerHTML = html['__html'];
    });
  }

  // initial state
  @override
  void initState() {
    super.initState();
    this.getListVideo();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 300.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(StringApp.TitleMenuProfile,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: data['image_menu'] == null ? null : Image.network(
                    NetworkApp.imageUrl + data['image_menu'],
                    fit: BoxFit.cover,
                  )),
            ),
          ];
        },
        body: Container(
          padding: EdgeInsets.all(10),
          child: Center(
            child: HtmlView(data: innerHTML),
          ),
        ),
      ),
    );
  }
}
