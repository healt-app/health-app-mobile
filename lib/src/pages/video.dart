import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:healt_app/src/components/cardVideo.dart';
import 'package:healt_app/src/components/loading/loadingVideoCard.dart';
import 'package:healt_app/src/pages/detailVideo.dart';
import 'package:healt_app/src/services/config.dart';
import 'package:http/http.dart' as http;

class Video extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: ColorApp.PrimaryDark,
          title: Text(StringApp.TitleMenuVideo)),
      body: ListVideo(),
    );
  }
}

class ListVideo extends StatefulWidget {
  @override
  _ListVideoState createState() => _ListVideoState();
}

class _ListVideoState extends State<ListVideo> {
  List data = [];

  Future getListVideo() async {
    String url = NetworkApp.listVideo;

    var res = await http.get(Uri.encodeFull(url));
    var content = json.decode(res.body);
    setState(() {
      data = content['payload'];
    });
  }

  // initial state
  @override
  void initState() {
    super.initState();
    this.getListVideo();
  }

  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: data.length <= 0 ? 3 : data.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(15),
          child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailVideo(
                        idVideo: data[index]['video_link'],
                        title: data[index]['title']),
                  ),
                );
              },
              child: data.length <= 0
                  ? (LoadingVideoCard())
                  : (VideoCard(
                      title: data[index]['title'],
                      description: data[index]['description'],
                      image: data[index]['video_image'],
                    ))),
        );
      },
    );
  }
}
