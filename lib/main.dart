import 'package:flutter/material.dart';
import 'package:healt_app/app.dart';
// import 'package:healt_app/src/components/inAppBrowser.dart';
// import 'package:healt_app/src/pages/home.dart';
import 'package:healt_app/src/services/config.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Healt App',
      theme: ThemeData(
          primarySwatch: Colors.pink,
          scaffoldBackgroundColor: ColorApp.LightGrey),
      home: App(),
      // routes: <String, WidgetBuilder>{
      //   '/home': (BuildContext context) => HomePage(),
      //   '/detail': (BuildContext context) => InAppBrowserMenu()
      // },
    );
  }
}
